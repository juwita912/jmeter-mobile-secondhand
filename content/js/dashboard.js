/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 6.54911838790932, "KoPercent": 93.45088161209068};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.01343408900083963, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "GET Buyer Product ID"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Order by ID"], "isController": false}, {"data": [0.12857142857142856, 500, 1500, "POST Login"], "isController": false}, {"data": [0.0, 500, 1500, "GET Seller Product"], "isController": false}, {"data": [0.0, 500, 1500, "GET Seller Product by ID"], "isController": false}, {"data": [0.0, 500, 1500, "POST Seller Product"], "isController": false}, {"data": [0.0, 500, 1500, "DELETE Product by ID"], "isController": false}, {"data": [0.0, 500, 1500, "PUT Buyer Order by ID"], "isController": false}, {"data": [0.0, 500, 1500, "POST Registration"], "isController": false}, {"data": [0.024752475247524754, 500, 1500, "GET Buyer Product"], "isController": false}, {"data": [0.0, 500, 1500, "POST Buyer Order"], "isController": false}, {"data": [0.0, 500, 1500, "GET Buyer Order"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1191, 1113, 93.45088161209068, 4561.19143576826, 12, 171246, 191.0, 13798.599999999997, 32040.8, 77226.07999999895, 3.5966443398904397, 1249.7601959893716, 66.68065255521046], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["GET Buyer Product ID", 98, 81, 82.65306122448979, 20961.051020408162, 13, 171246, 110.5, 98824.5, 120184.75, 171246.0, 0.29975285682824776, 1264.7779424910382, 0.06834288668118531], "isController": false}, {"data": ["GET Buyer Order by ID", 92, 92, 100.0, 4578.489130434782, 13, 60169, 15.0, 15408.800000000007, 43030.19999999999, 60169.0, 0.3277427077247531, 0.10175164227595936, 0.0767451185929863], "isController": false}, {"data": ["POST Login", 105, 75, 71.42857142857143, 1444.3142857142857, 13, 35784, 16.0, 1114.0000000000005, 12649.19999999988, 35420.459999999985, 0.3214449716822287, 0.11324716822286852, 0.0841281761824583], "isController": false}, {"data": ["GET Seller Product", 103, 103, 100.0, 4279.601941747573, 13, 44717, 26.0, 28714.000000000007, 38867.0, 44668.91999999999, 0.3320791702534441, 0.10494598474370259, 0.08082843274751988], "isController": false}, {"data": ["GET Seller Product by ID", 103, 103, 100.0, 2489.378640776699, 13, 43513, 24.0, 7851.800000000027, 15917.8, 43503.36, 0.3162283475687259, 0.09993669676464628, 0.0772791419389403], "isController": false}, {"data": ["POST Seller Product", 104, 104, 100.0, 3097.5384615384614, 60, 37272, 257.0, 6329.0, 31280.25, 37256.75, 0.31862061781763257, 0.10064900281703518, 33.255970139465454], "isController": false}, {"data": ["DELETE Product by ID", 102, 102, 100.0, 2397.9117647058824, 13, 32242, 194.5, 7939.8, 14318.399999999994, 32144.529999999995, 0.3150608345405517, 0.09950623094576938, 0.08346180138348772], "isController": false}, {"data": ["PUT Buyer Order by ID", 90, 90, 100.0, 2111.0000000000005, 13, 42002, 17.5, 743.200000000003, 28214.95, 42002.0, 0.4805843906189927, 0.1488268067303174, 0.1349036261427229], "isController": false}, {"data": ["POST Registration", 105, 105, 100.0, 1335.8666666666668, 60, 33500, 254.0, 1173.0000000000002, 3891.099999999981, 33305.05999999999, 0.3533557013101083, 0.11081802686176388, 36.9249806390522], "isController": false}, {"data": ["GET Buyer Product", 101, 70, 69.3069306930693, 3874.950495049505, 13, 36774, 20.0, 13886.199999999999, 29232.799999999934, 36732.68000000001, 0.3109672930143199, 0.09088120164227677, 0.09331063361895114], "isController": false}, {"data": ["POST Buyer Order", 95, 95, 100.0, 1976.9368421052634, 12, 34960, 48.0, 5257.8000000000075, 17269.799999999985, 34960.0, 0.31174729436164, 0.10386235413508176, 0.09067526412379322], "isController": false}, {"data": ["GET Buyer Order", 93, 93, 100.0, 6795.172043010752, 13, 76076, 18.0, 35062.60000000003, 52339.99999999997, 76076.0, 0.30766991319076853, 0.09579792785967606, 0.07164813996169013], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 889, 79.87421383647799, 74.6431570109152], "isController": false}, {"data": ["504/Gateway Time-out", 3, 0.2695417789757412, 0.2518891687657431], "isController": false}, {"data": ["500/Internal Server Error", 30, 2.6954177897574123, 2.5188916876574305], "isController": false}, {"data": ["403/Forbidden", 168, 15.09433962264151, 14.105793450881611], "isController": false}, {"data": ["404/Not Found", 20, 1.7969451931716083, 1.6792611251049538], "isController": false}, {"data": ["Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: The target server failed to respond", 3, 0.2695417789757412, 0.2518891687657431], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1191, 1113, "502/Bad Gateway", 889, "403/Forbidden", 168, "500/Internal Server Error", 30, "404/Not Found", 20, "504/Gateway Time-out", 3], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["GET Buyer Product ID", 98, 81, "502/Bad Gateway", 75, "504/Gateway Time-out", 3, "Non HTTP response code: org.apache.http.NoHttpResponseException/Non HTTP response message: The target server failed to respond", 3, "", "", "", ""], "isController": false}, {"data": ["GET Buyer Order by ID", 92, 92, "502/Bad Gateway", 80, "403/Forbidden", 12, "", "", "", "", "", ""], "isController": false}, {"data": ["POST Login", 105, 75, "502/Bad Gateway", 75, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["GET Seller Product", 103, 103, "502/Bad Gateway", 70, "403/Forbidden", 33, "", "", "", "", "", ""], "isController": false}, {"data": ["GET Seller Product by ID", 103, 103, "502/Bad Gateway", 70, "403/Forbidden", 33, "", "", "", "", "", ""], "isController": false}, {"data": ["POST Seller Product", 104, 104, "502/Bad Gateway", 71, "403/Forbidden", 33, "", "", "", "", "", ""], "isController": false}, {"data": ["DELETE Product by ID", 102, 102, "502/Bad Gateway", 70, "403/Forbidden", 32, "", "", "", "", "", ""], "isController": false}, {"data": ["PUT Buyer Order by ID", 90, 90, "502/Bad Gateway", 80, "403/Forbidden", 10, "", "", "", "", "", ""], "isController": false}, {"data": ["POST Registration", 105, 105, "502/Bad Gateway", 75, "500/Internal Server Error", 30, "", "", "", "", "", ""], "isController": false}, {"data": ["GET Buyer Product", 101, 70, "502/Bad Gateway", 70, "", "", "", "", "", "", "", ""], "isController": false}, {"data": ["POST Buyer Order", 95, 95, "502/Bad Gateway", 75, "404/Not Found", 20, "", "", "", "", "", ""], "isController": false}, {"data": ["GET Buyer Order", 93, 93, "502/Bad Gateway", 78, "403/Forbidden", 15, "", "", "", "", "", ""], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
